require('jquery');
var _browserWidth = window.innerWidth;

var aboutMap = {
    showMap: function(locations) {
        var t = locations;
        locations = t;
        var e = document.getElementById("map");
        if ("undefined" != typeof e && null != e) {
            var i, n, s = new google.maps.Map(document.getElementById("map"), {
                zoom: 3.5,
                disableDefaultUI: !0,
                draggable: !1,
                scrollwheel: !1,
                center: new google.maps.LatLng(57.220946, 13.8236),
                styles: [{
                    elementType: "geometry",
                    stylers: [{
                        color: "#f5f5f5"
                    }]
                }, {
                    elementType: "labels.icon",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#125976"
                    }]
                }, {
                    elementType: "labels.text.stroke",
                    stylers: [{
                        color: "#f5f5f5"
                    }]
                }, {
                    featureType: "administrative.country",
                    elementType: "geometry",
                    stylers: [{
                        color: "#e5e5e5"
                    }]
                }, {
                    featureType: "landscape",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#f2f2f2"
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "poi",
                    elementType: "geometry",
                    stylers: [{
                        color: "#eeeeee"
                    }]
                }, {
                    featureType: "poi",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#757575"
                    }]
                }, {
                    featureType: "poi.park",
                    elementType: "geometry",
                    stylers: [{
                        color: "#e5e5e5"
                    }]
                }, {
                    featureType: "poi.park",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#9e9e9e"
                    }]
                }, {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "road.highway",
                    stylers: [{
                        visibility: "on"
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry",
                    stylers: [{
                        color: "#fcfcfc"
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "water",
                    elementType: "geometry",
                    stylers: [{
                        color: "#c9c9c9"
                    }]
                }, {
                    featureType: "water",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#dde6e8"
                    }]
                }, {
                    featureType: "water",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#9e9e9e"
                    }]
                }]
            }),
            o = new google.maps.InfoWindow,
            r = new google.maps.LatLngBounds;
            for (n = 0; n < locations.length; n++) {
                var a = locations[n][3].toString();
                i = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[n][1], locations[n][2]),
                    label: {
                        text: a,
                        color: "white"
                    },
                    map: s,
                    icon: "/assets/images/map-pin-color.png"
                }), r.extend(i.getPosition()), google.maps.event.addListener(i, "click", function(t, e) {
                    return function() {
                        o.setContent(locations[e][0]), o.open(s, t)
                    }
                }(i, n))
            }
            // s.fitBounds(r)
        }
    },
    init: function(l) {
        null != document.getElementById("map") && this.showMap(l)
    }
};

var playBtn = {
    playButtonClass: ".play-btn",
    openPlayer: function(t) {
        u.cleanUp();
        var e = $(this).data("youtubeId"),
        i = new h(e);
        i.open()
    },
    bindPlayers: function() {
        $(this.playButtonClass).on("touchstart click", this.openPlayer)
    },
    init: function() {
        this.bindPlayers()
    }
};

var language = {
    toggleLanguageSelect: function(el) {
        el.is(":checked") ? $("#contact-speaks-language-button").css("display", "inline-block") : ($("#contact-speaks-language-button").hide(), $("#contact-speaks-language").val(null))
    },
    bindEvents: function() {
        var _this = this;
        $("#contact-does-not-speaks-swedish").on("click", function(){
            _this.toggleLanguageSelect($(this));
        });
        _this.toggleLanguageSelect($("#contact-does-not-speaks-swedish"));
    },
    init: function() {
        this.bindEvents()
    }
};

var callMe = {
    toggleCallMeSelect: function(el) {
        el.is(":checked") ? $("#contact-time-button").css("display", "inline-block") : ($("#contact-time-button").hide(), $("#contact-time").val(null))
    },
    bindEvents: function() {
        var _this = this;
        $("#contact-call-me").on("click", function(){
            _this.toggleCallMeSelect($(this));
        });
        _this.toggleCallMeSelect($('#contact-call-me'));
    },
    init: function() {
        $("#contact-time-button").hide();
        this.bindEvents()
    }
};

var emailConfirmation = {
    fields: function() {
        return o.getWhere(/^session_/)
    },
    showConfirmationMessage: function() {
        var t = this.fields(),
        e = $("#email-confirmation-message");
        1 == t.session_confirmed && (e.find(".password").text(Utils.decodeUriString(t.session_password)), e.show(), o.remove("session_confirmed"), o.remove("session_password"))
    },
    showPasswordResetButton: function() {
        var t = this.fields(),
        e = $("#password-reset-message");
        1 == t.session_password_reset && (e.find(".password").text(Utils.decodeUriString(t.session_password)), e.show(), o.remove("session_password_reset"), o.remove("session_password"))
    },
    init: function() {
        this.showConfirmationMessage(), this.showPasswordResetButton()
    }
};

var s = {
    cookiesAccepted: function() {
        return 1 == o.get().cookies_accepted
    },
    acceptCookies: function() {
        o.set("cookies_accepted", 1, 31536e3)
    },
    show: function() {
        $(".cookie-consent").show()
    },
    hide: function() {
        $(".cookie-consent").hide()
    },
    bind: function() {
        var t = this;
        $(".cookie-consent .accept-button").on("click", function() {
            t.acceptCookies(), t.hide()
        })
    },
    init: function() {
        this.bind(), this.cookiesAccepted() || this.show()
    }
};

var o = {
    getWhere: function(t) {
        var e = this.get(),
        i = Object.keys(e).filter(function(e) {
            return e.match(t)
        }),
        n = {};
        for (var s in i) {
            var o = i[s];
            n[o] = e[o]
        }
        return n
    },
    domain: function() {
        return "." + window.location.hostname.split(".").slice(-2).join(".")
    },
    set: function(t, e, i) {
        i = i || "", document.cookie = encodeURIComponent(t) + "=" + e + "; expires=" + i + "; domain=" + this.domain() + "; path=/"
    },
    remove: function(t) {
        this.set(t, "", "Thu, 01 Jan 1970 00:00:00 GMT")
    },
    get: function() {
        for (var t = document.cookie.split(";"), e = {}, i = 0; i < t.length; i++) {
            var n = t[i].indexOf("="),
            s = t[i].substr(0, n).trim(),
            o = t[i].substr(n + 1);
            e[s] = o
        }
        return e
    },
    signedIn: function() {
        var t = this.get();
        return t.session_userID > 0
    }
};


var topSubMenu = {
    browserWidth: window.innerWidth,
    elems : function() {
        return {
            topSub: $(".top-sub-menu-wrapper"),
            topSubListEls: $(".top-sub-menu li"),
            topSubMenuSection: $(".top-menu-section"),
        }
    },
    clickTopSubMenu: function(t) {
        t.preventDefault();
        var e, i, n, s;
        e = $(t.currentTarget), i = e.attr("href"), i && (n = $(i), s = this.elems().topSub.hasClass("sticky") ? 158 : 116, Utils.scrollToElem(n, s), this.browserWidth < 769 && this.toggleTopSubMenu())
    },
    toggleTopSubMenu: function(t) {
        var e, i;
        i = $(".top-sub-menu-wrapper "), e = i.find("ul"), i.toggleClass("menu-open"), e.slideToggle({
            duration: 200
        }), this.animatingTopMenu = !0
    },
    bindEvents: function() {
        this.browserWidth < 767 && $(".top-sub-menu-wrapper p").on("click", this.toggleTopSubMenu), $(".top-sub-menu a").on("click", this.clickTopSubMenu.bind(this));
    },
    stickyTopSubMenu: function() {
        if (this.elems().topSub.length > 0) {
            var t, e;
            t = this, e = $(document).scrollTop();
            var i = function(e) {
                t.elems().topSubMenuSection.each(function(i, n) {
                    var s, o, r;
                    s = $(n), o = s.offset().top - 180, r = o + s.outerHeight(), e >= o && e <= r && (t.elems().topSubListEls.removeClass("selected"), $("#" + s.attr("id") + "-li").addClass("selected"))
                })
            };
            $(document).on("scroll", function() {
                t.browserWidth < 767 && ($(this).scrollTop() > 100 ? t.elems().topSub.addClass("sticky") : t.elems().topSub.removeClass("sticky")), i($(this).scrollTop())
            }), $(".top-sub-go-top").on("click", function(t) {
                Utils.scrollToElem($(".page-wrapper")), t.preventDefault()
            }), i($(this).scrollTop()), e > 100 && (t.browserWidth > 767 && t.elems().topSub.addClass("sticky"), i(e))
        }
    },
    init: function() {
        this.bindEvents(); this.stickyTopSubMenu();
    }

};


var t = {
    elems: {
        jobDescription: $(".job"),
        jobSent: $(".job-sent"),
        addFileBtn: $(".add-file"),
        fileAttachement: $(".file-attachements"),
        forms: $(".job-form")
    },
    files: [],
    submit: function() {
        this.elems.forms.on("submit", function(t) {
            Utils.validateForm($(t.currentTarget)) ? console.log("Form is valid!") : t.preventDefault()
        })
    },
    triggerClick: function() {
        $(".add-file").on("click", function(t) {
            $(this).siblings(".file-attachements").trigger("click"), t.preventDefault()
        })
    },
    checkIsSent: function() {
        var t = Utils.getParameterByName("applicationsent");
        t ? this.elems.jobSent.show() : this.elems.jobDescription.show()
    },
    fileChange: function() {
        this.elems.fileAttachement.change(function(t) {
            var e = this.files[0];
            e && $(this).siblings(".file-name").html(e.name)
        })
    },
    init: function() {
        this.elems.forms.length > 0 && (this.checkIsSent(), this.triggerClick(), this.fileChange(), this.submit())
    }
};


var howItWorksCarouselInit = {
    browserWidth: window.innerWidth,
    howItWorksCarousel: function() {
        var t, e;
        t = $(".how-it-works-carousel"), e = $(".how-it-works-carousel-btn"), t.slick({
            appendArrows: !1,
            draggable: !1,
            fade: !0
        });
        var i = function(i) {
            var n, s, o;
            n = $(i.currentTarget), s = n.data("num"), o = "active", e.removeClass(o), n.addClass(o), t.slick("slickGoTo", s)
        };
        e.on("click", i), e.on("mouseover", i), $(e[0]).addClass("active")
    },
    submitContactForm: function(t) {
        var e = $(".contact-spinner"),
        i = function() {
            var t, i, n, s, o, r, a, l, c;
            t = $("#contact-name").val(), i = $("#contact-phone").val(), n = $("#contact-email").val(), s = $("#contact-message").val(), a = $("#contact-send-to").val(), o = $("#contact-call-me").is(":checked"), r = $("#contact-time").find(":selected").text(), l = $("#contact-form [name=speaks-swedish]").val(), c = $("#contact-form [name=speaks-language]").val(),
            $.ajax({
                method: "POST",
                url: ajaxurl,
                data: {
                    action: "post_contact_form",
                    name: t,
                    phone: i,
                    email: n,
                    message: s,
                    callMe: o,
                    time: r,
                    sendTo: a,
                    speaksSwedish: l,
                    language: c
                },
                success: function(t) {
                    $(".success-msg").show(), e.hide()
                },
                error: function(t) {
                    e.hide(), $(".error-msg").show()
                }
            })
        };
        Utils.validateForm($(t.currentTarget)) && (i(), e.show(), $(t.currentTarget).hide()), t.preventDefault()
    },
    submitInsuranceContactForm: function(t) {
        var e = $(".contact-spinner"),
        i = function() {
            var t, i, n, s, o;
            t = $("#contact-name").val(), i = $("#contact-phone").val(), n = $("#contact-email").val(), s = $("#contact-message").val(), o = $("#contact-send-to").val(), $.ajax({
                method: "POST",
                url: ajaxurl,
                data: {
                    action: "post_insurance_form",
                    name: t,
                    phone: i,
                    email: n,
                    message: s,
                    sendTo: o
                },
                success: function(t) {
                    $(".success-msg").show(), e.hide()
                },
                error: function(t) {
                    e.hide(), $(".error-msg").show()
                }
            })
        };
        Utils.validateForm($(t.currentTarget)) && (i(), e.show(), $(t.currentTarget).hide()), t.preventDefault()
    },
    submitCompanyContactForm: function(t) {
        var e = $(".contact-spinner"),
        i = function() {
            var t, i, n, s, o;
            t = $("#contact-name").val(), i = $("#contact-phone").val(), n = $("#contact-email").val(), s = $("#contact-message").val(), o = $("#contact-send-to").val(), $.ajax({
                method: "POST",
                url: ajaxurl,
                data: {
                    action: "post_company_form",
                    name: t,
                    phone: i,
                    email: n,
                    message: s,
                    sendTo: o
                },
                success: function(t) {
                    $(".success-msg").show(), e.hide()
                },
                error: function(t) {
                    e.hide(), $(".error-msg").show()
                }
            })
        };
        Utils.validateForm($(t.currentTarget)) && (i(), e.show(), $(t.currentTarget).hide()), t.preventDefault()
    },
    submitLoginForm: function(t) {},
    networkShowMoreBtn: function() {
        $(".show-more").on("click", function(t) {
            var e, i;
            e = $(t.currentTarget), i = e.data("section"), $("." + i).show(), e.hide()
        })
    },
    init: function() {
        $("#contact-form").on("submit", this.submitContactForm), $("#insurance-form").on("submit", this.submitInsuranceContactForm), $("#login-form").on("submit", this.submitLoginForm), $("#company-form").on("submit", this.submitCompanyContactForm), this.browserWidth > 767 && this.howItWorksCarousel(), this.networkShowMoreBtn()
    }
};

var headerInit = {
    elems: {
        menu: $(".top-menu-wrapper"),
        header: $(".page-header"),
        window: $(window)
    },
    updateMenuSize: function() {
        var t = this.elems.header.outerHeight(),
        e = this.elems.window.innerHeight();
        this.elems.menu.css("height", e - t)
    },
    init: function() {
        this.updateMenuSize(),   this.elems.window.on("resize", this.updateMenuSize.bind(this))
    }
},
c = function() {
    var t = function(t) {
        this.id = null, this.elem = null, this.settings = "autoplay=1&modestbranding=1&rel=0&showinfo=0&autohide=1&color=white&iv_load_policy=3", this.construct = function(t) {
            this.id = t, this.build()
        }, this.construct(t)
    };
    return t.prototype.build = function() {
        var t = $("<iframe>");
        return t.attr("src", this.src()), t.attr("frameborder", 0), t.attr("allowfullscreen", "allowfullscreen"), this.elem = t, t
    }, t.prototype.src = function() {
        return "https://www.youtube.com/embed/" + this.id + "?" + this.settings
    }, t
}(window),
u = function() {
    var t = function() {
        this.elem = null, this.construct = function() {
            this.build(), this.bindEvents()
        }, this.bindEvents = function() {
            this.elem.on("click", ".close", this.onClose.bind(this))
        }, this.onClose = function() {
            this.close()
        }, this.construct()
    };
    return t.prototype.build = function() {
        var t = $("<div>");
        t.addClass("modal"), t.data("modal", this);
        var e = $("<div>");
        return e.addClass("close"), t.append(e), this.elem = t, t
    }, t.prototype.open = function() {
        $("body").append(this.elem), this.elem.show()
    }, t.prototype.close = function() {
        this.elem.remove()
    }, t.cleanUp = function() {
        $(".modal").remove()
    }, t
}(window),
h = function() {
    var t = function(t) {
        this.construct = function(t) {
            this.player = new c(t), u.call(this)
        }, this.construct(t)
    };
    return t.prototype = Object.create(u.prototype), t.prototype.build = function() {
        u.prototype.build.call(this), this.elem.addClass("modal-player");
        var t = $("<div>");
        t.addClass("inner"), this.elem.append(t.html(this.player.elem))
    }, t
}(window);

var Utils = {
    validateForm: function(t) {
        var e, i, n, s;
        return e = t.find("input"), i = !0, n = t.find("textarea"), s = function(t) {
            t.prop("required") && t.val().length < t.prop("minLength") ? (t.next().show(), i = !1) : t.prop("required") && t.val().length >= t.prop("minLength") && t.next().hide()
        }, e.each(function(t, e) {
            s($(e))
        }), n.each(function(t, e) {
            s($(e))
        }), i
    },
    scrollToElem: function(t, e) {
        e || (e = 0), $("html, body").css("height", "auto"), $("html, body").animate({
            scrollTop: t.offset().top - e
        }, 1e3)
    },
    validateDecimal: function(t) {
        var e = t.toString();
        return parseFloat(e.replace(/,/g, "."))
    },
    getParameterByName: function(t, e) {
        e || (e = window.location.href), t = t.replace(/[\[\]]/g, "\\$&");
        var i = new RegExp("[?&]" + t + "(=([^&#]*)|&|#|$)"),
        n = i.exec(e);
        return n ? n[2] ? decodeURIComponent(n[2].replace(/\+/g, " ")) : "" : null
    },
    formatNumber: function(t, e) {
        return t.toFixed(e).replace(/./g, function(t, e, i) {
            return e && "." !== t && (i.length - e) % 3 === 0 ? " " + t : t
        })
    },
    toMoney: function(t) {
        return this.formatNumber(t, 0)
    },
    between: function(t, e, i) {
        return Math.max(e, Math.min(i, t))
    },
    decodeUriString: function(t) {
        t = t.replace(/\+/g, "%20");
        for (var e = t.split("%"), i = e[0], n = 1; n < e.length; n++) i += String.fromCharCode(parseInt(e[n].substring(0, 2), 16)) + e[n].substring(2);
        return i
    }
};

var newsAndPress = {
    offset: 3,
    size: 3,
    pressOffset: 3,
    pressSize: 3,
    el: $("#news-list"),
    pressEl: $("#press-list"),
    btn: $("#show-more-news"),
    pressBtn: $("#show-more-press"),
    getNews: function(t) {
        var e, i, n, s, o;
        e = this, s = $(t.currentTarget), o = s.parents(".list-content"), i = o.find(".spinner"), n = o.find(".error"), i.show(), $.ajax({
            method: "GET",
            url: ajaxurl,
            data: {
                action: "get_news",
                offset: this.offset,
                size: this.size
            },
            success: function(t) {
                var s = JSON.parse(t);
                e.renderNews(s, e.el), n.hide(), e.offset += e.size, s.length < e.size && e.btn.hide(), i.hide()
            },
            error: function(t) {
                console.log(t), t.show(), i.hide()
            }
        })
    },
    getPress: function(t) {
        var e, i, n, s, o;
        e = this, s = $(t.currentTarget), o = s.parents(".list-content"), i = o.find(".spinner"), n = o.find(".error"), i.show(), $.ajax({
            method: "GET",
            url: ajaxurl,
            data: {
                action: "get_press",
                offset: this.pressOffset,
                size: this.pressSize
            },
            success: function(t) {
                var s = JSON.parse(t);
                e.renderNews(s, e.pressEl), n.hide(), e.pressOffset += e.pressSize, s.length < e.pressSize && e.pressBtn.hide(), i.hide()
            },
            error: function(t) {
                console.log(t), t.show(), i.hide()
            }
        })
    },
    renderNews: function(t, e) {
        for (var i = "", n = 0; n < t.length; n++) i += "<li>", i += "<h3>" + t[n].title + "</h3>", i += '<div class="text-wrapper">' + t[n].text + "</div>", "" !== t[n].url && (i += '<a href="' + t[n].url + '" class="pointer-link" target="_blank">' + t[n].url_text + "</a>"), i += "</li>";
        e.append(i)
    },
    init: function() {
        this.el.length > 0 && (this.size = parseInt(this.el.data("size")), this.offset = this.size), this.btn.on("click", this.getNews.bind(this)), this.pressBtn.on("click", this.getPress.bind(this))
    }
};

var menuToggle = {
    browserWidth: window.innerWidth,
    elems : function() {
        return {
            header: $(".page-header"),
            menuWrapper: $(".top-menu-wrapper"),
            pageWrapper: $(".page-wrapper"),
            topSub: $(".top-sub-menu-wrapper"),
            topSubListEls: $(".top-sub-menu li"),
            topSubMenuSection: $(".top-menu-section")
        }
    },
    toggleMenu: function(t) {
        this.elems().pageWrapper.toggleClass("menu-open"), t && t.stopPropagation()
    },
    stopMenuPropagation: function(t) {
        t.stopPropagation()
    },
    closeAllMenus: function(t) {
        $(".select-list-sub").slideUp().removeClass("shown"), this.elems().header.removeClass("menu-open"), this.elems().pageWrapper.hasClass("menu-open") && this.toggleMenu()
    },
    styleSelects: function() {
        //$(".styled-select").selectmenu()
    },
    toggleSubMenu: function(t) {
        $(t.currentTarget).siblings("ul").toggleClass("open"), t.preventDefault()
    },
    toggleElement: function(t) {
        $(".toggle-element").on("click", function() {
            var t, e;
            t = $(this), e = $("#" + t.data("element")), e.is(":visible") ? e.hide() : e.show()
        })
    },
    stickyHeader: function() {
        var t, e, i, n;
        if (t = $(".page-header"), e = $(".start-page-hero").length > 0, i = 600, n = "25, 104, 137,", e = !1) {
            var s = function(e) {
                e > 5 && t.hasClass("transparent-bg") ? t.removeClass("transparent-bg") : e <= 5 && t.addClass("transparent-bg")
            };
            $(document).on("scroll", function() {
                s($(this).scrollTop())
            }), s($(document).scrollTop())
        }
    },
    clickScrollToLink: function(t) {
        var e, i;
        e = $(t.currentTarget), i = $("#" + e.attr("href")), Utils.scrollToElem(i), t.preventDefault()
    },
    initTabs: function() {
        var t, e, i;
        t = $(".tabs .tab-btn"), e = $(".tab-content"), i = function(t) {
            var e, i;
            e = $(t.currentTarget), i = e.data("id"), $(this).parents(".tabs").find(".tab-btn").removeClass("active"), e.addClass("active"), $(this).parents(".tabs").find(".tab-content").hide(), $("#tab-" + i).show()
        }, t.on("click", i), $(".tab-selector").selectmenu({
            select: function(t, e) {
                $(this).parents(".tabs").find(".tab-content").hide(), $("#tab-" + e.item.value).show()
            }
        })
    },
    bindEvents: function() {
        $("body").on("click", this.closeAllMenus.bind(this)), $(".sub-menu-link").on("click", this.toggleSubMenu), $(".scroll-to-elem").on("click", this.clickScrollToLink), $(".top-menu-wrapper").on("click", this.stopMenuPropagation.bind(this))
    },
    init: function() {
        this.bindEvents(), this.styleSelects(), this.initTabs(), this.stickyHeader(), this.toggleElement()
    }
};

var headerSelectList = {
    toggleDropDown: function(t) {
        var e, i;
        e = $(t.currentTarget), i = e.find(".select-list-sub"), i.hasClass(".shown") ? i.slideUp("fast") : i.slideDown("fast"), i.toggleClass(".shown"), t.stopPropagation()
    },
    bindEvents: function() {
        $(".select-list").on("click", this.toggleDropDown);
    },
    init: function() {
        this.bindEvents();
    }
};

var headerMenuBtn = {
    elems : function() {
        return {
            pageWrapper: $(".page-wrapper"),
        }
    },
    toggleMenu: function(t) {
        this.elems().pageWrapper.toggleClass("menu-open"), t && t.stopPropagation()
    },
    bindEvents: function() {
        $(".menu-btn,.menu-list li a").on("click", this.toggleMenu.bind(this));
    },
    init: function() {
        this.bindEvents();
    }
};

var  initialiseAccordian =
{
    noResultStr: null,
    getQueryParams: function() {
        for (var t, e = location.search.split("+").join(" "), i = {}, n = /[?&]?([^=]+)=([^&]*)/g; t = n.exec(e);) i[decodeURIComponent(t[1])] = decodeURIComponent(t[2]);
        return i
    },
    toggleAccordion: function(t, e) {
        return "child" == e ? (t.parent().toggleClass("open"), t.parent().next().slideToggle()) : (t.toggleClass("open"), t.next().slideToggle()), !1
    },
    findQAParam: function(id) {
        // var t = this.getQueryParams();
        id && this.autoOpenAccordion(id)
    },
    autoOpenAccordion: function(t) {
        var e, i, n;
        e = $(".accordion").find("[data-qa-id='" + t + "']"), i = $(e[0]).parent(), n = i.parents("dd"), n.prev().hasClass("open") || (n.prev().addClass("open"), n.slideToggle()), i.hasClass("open") || (i.addClass("open"), i.next().slideToggle()), $("html, body").css("height", "auto"), $("html, body").animate({
            scrollTop: e.offset().top - 120
        }, 1e3)
    },
    questionFormsubmit: function(){
        var _this = this;
        $("#questionSearchButton").on("click", function(){
          var _autocompletefield = $('#autocomplete-field').val();
          var i = $(".accordion .accordion a"), n = [];
          $.each(i, function(t, e) {
              var s = $(e),text = s.text(),idn = s.data("qa-id");
              if (text.toLowerCase().indexOf(_autocompletefield.toLowerCase()) !== -1) {
                 _this.autoOpenAccordion(s.data("qa-id"));
               }
          });
          var _questionId = $('#question_id').val();
          _questionId && _this.autoOpenAccordion(_questionId);
        });
    },
    autoCompleteSearch: function() {
        var t, e;
        if (t = $("#autocomplete-field"), e = this, t.length > 0) {
            var i, n, s, text, idn;
            i = $(".accordion .accordion a"), n = [], $.each(i, function(t, e) {
                s = $(e), text = s.text(), idn = s.data("qa-id");
                var i = n.map(function(t) {
                    return t.id
                });
                if (Array.prototype.some) var o = i.some(function(t) {
                    return t == idn
                });
                else var o = !1;
                o || n.push({
                    label: text,
                    id: idn
                })
            }), t.autocomplete({
                source: n,
                minLength: 2,
                focus: function( event, i ) {
                    $('#question_id').val(i.item.id);
                },
                select: function(t, i) {
                    i.item.id && e.autoOpenAccordion(i.item.id)
                },
                response: function(t, i) {

                    if (!i.content.length && e.noResultStr) {
                        var n = {
                            value: "",
                            label: e.noResultStr
                        };
                        i.content.push(n)
                    }
                }
            })
        }
    },
    setNoResultString: function() {
        var t = $(".search-wrapper");
        t.length > 0 && (this.noResultStr = t.data("no-results"))
    },
    submitForm: function(t) {
        var e = $(".faq-spinner"),
        i = function() {
            var t, i, n, s, o;
            t = $("#faq-form-name").val(), i = $("#faq-form-phone").val(), n = $("#faq-form-email").val(), s = $("#faq-form-message").val(), o = $("#faq-send-to").val(), $.ajax({
                method: "POST",
                url: ajaxurl,
                data: {
                    action: "post_faq_form",
                    name: t,
                    phone: i,
                    email: n,
                    message: s,
                    sendTo: o
                },
                success: function(t) {
                    $(".success-msg").show(), e.hide()
                },
                error: function(t) {
                    e.hide(), $(".error-msg").show()
                }
            })
        };
        Utils.validateForm($(t.currentTarget)) && (i(), e.show(), $(t.currentTarget).hide()), t.preventDefault()
    },
    init: function() {
        this.setNoResultString(), this.autoCompleteSearch(),this.questionFormsubmit()
    }
};

function stickyHowItWorksCarousel() {
    var t, e, i = $(".page-header"),
    n = $(".top-sub-menu-wrapper"),
    s = $(".how-it-works-page .guide .container .col-sm-7"),
    o = $(".how-it-works-page .guide .container .col-sm-5"),
    r = $(".how-it-works-page .guide .container .col-sm-5 .how-it-works-carousel"),
    a = {},
    l = 15;
    r.width(r.width()), o.height(s.height()), e = "absolute" === n.css("position") ? i.height() : i.height() + n.height(), $(document).on("scroll", function() {
        a.top = s.offset().top - e - l, a.bottom = a.top + s.height() - r.outerHeight(), t = $(this).scrollTop(), t > a.top && t < a.bottom ? (r.addClass("sticky").removeClass("bottom"), r.css({
            top: e + l,
            bottom: ""
        })) : t >= a.bottom ? (r.removeClass("sticky").addClass("bottom"), r.css({
            top: "",
            bottom: 0
        })) : (r.removeClass("sticky").removeClass("bottom"), r.css({
            top: 0,
            bottom: ""
        }))
    })
};

function testimonialSlider() {
    var  t = $(".testimonial-slider");
    t.slick({
        appendArrows: false,
        slidesToShow: 1,
        dots : true,
        autoplay: true,
        autoplaySpeed: 5e3,
        dotsClass: "slick-dots",
        asNavFor: ".testimonial-background-wrapper",
    });

    var e = $(".testimonial-background-wrapper");
    e.slick({
        appendArrows: false,
        slidesToShow: 1,
        draggable: false,
        fade: true,
        autoplay: true,
        autoplaySpeed: 5e3,
        asNavFor: ".testimonial-slider",
    });

    e.slick('slickAdd','<div class="background-image testimonial-first" style="background-image: url(/assets/images/ff_matsj.jpg);"></div>');
    e.slick('slickAdd','<div class="background-image testimonial-second" style="background-image: url(/assets/images/ff_lisaa-11-1.jpg);" ></div>');
    e.slick('slickAdd','<div class="background-image testimonial-third" style="background-image: url(/assets/images/ff_mats-2.jpg);" ></div>');
};

function startPageSlider(sliderParentClass) {
    var t, e;
    t = $("."+sliderParentClass), e = t.find("li").length, e > 1 && (t.slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: !0,
        appendArrows: !1,
        initialSlide: 1,
        infinite: !1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 590,
            centerMode: !1,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    }), $(".start-slider-next").on("click", function() {
        t.slick("slickNext")
    }), $(".start-slider-prev").on("click", function() {
        t.slick("slickPrev")
    }))
};

function scrollToMe(element,scrollParentClass = null)
{
    if (scrollParentClass == null) {
        scrollParentClass = 'html, body';
    }

    $(scrollParentClass).animate({ scrollTop : $(element).offset().top-100 }, 'slow');
}
function initPlaceholderEffect(){
    (function() {
        [].slice.call(document.querySelectorAll( 'input.input__field--style' ) ).forEach( function( inputEl ) {
            if( inputEl.value.trim() !== '' ) {
                inputEl.parentNode.classList.add('input--filled');
            }
            // events:
            inputEl.addEventListener( 'focus', onInputFocus );
            inputEl.addEventListener( 'blur', onInputBlur );
        } );

        function onInputFocus( ev ) {
            ev.target.parentNode.classList.add('input--filled');
        }

        function onInputBlur( ev ) {
            if( ev.target.value.trim() === '' ) {
                ev.target.parentNode.classList.remove('input--filled');
            }
        }
    })();
}

function initFancybox(){
    $(document).ready(function() {
        $('.fancybox').fancybox();
    });
}

module.exports = {
    _browserWidth,aboutMap,menuToggle,headerSelectList,headerMenuBtn,newsAndPress,headerInit,howItWorksCarouselInit,topSubMenu,emailConfirmation,callMe,language,playBtn,initialiseAccordian,stickyHowItWorksCarousel,testimonialSlider,startPageSlider,scrollToMe,initPlaceholderEffect,initFancybox
};
